# Currency Exchange and Tax Calculation

**Notes:**
1. currency-exchange-and-tax-calculation is also available running at https://currency-exchange-and-tax-calc.herokuapp.com/
2. This application uses Fixer.io API to retrieve exchange rates. If Fixer.io is not available, offline exchange rates are used.
3. Since the free subscription of Fixer.io doesnot provide access to 'https' API calls, we are unable to use that API via the deployed heroku application which is in 'https'.


**Prerequisites:**
1. Node Package manager should be installed. 
2. Latest Angular CLI should be installed >=10.

**Steps to setup:**

1. Clone this project.
2. Open command prompt and traverse to the project directory.
3. Execute this command to download project dependencies: npm install
4. Execute this command to run the project: ng serve



