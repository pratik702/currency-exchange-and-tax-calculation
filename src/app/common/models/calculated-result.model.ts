export class CalculatedResult{
    preTaxAmount: number;
    taxAmount: number;
    total: number;
    exchangeRate: number;

    constructor(preTaxAmount, taxAmount, total, exchangeRate){
        this.preTaxAmount = preTaxAmount;
        this.taxAmount = taxAmount;
        this.total = total;
        this.exchangeRate = exchangeRate;
    }
}