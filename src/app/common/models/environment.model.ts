export class Environment {
    environmentName: string;
    production: boolean;
    api: string;
    fixerApiKey: string;
  }