export class Currency{
    name: string;
    code: string;
    rate: number;
    tax: number;

    constructor(name, code, rate, tax){
        this.name = name;
        this.code = code;
        this.rate = rate;
        this.tax = tax;
    }
}