import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';

@Injectable()
export class NotificationService {

    constructor(private toastr: ToastrService) { }

    success(message, title) {
        this.toastr.success(title, message)
    }

    error(message, title) {
        this.toastr.error(title, message)
    }
    warn(message, title) {
        this.toastr.warning(title, message)
    }
}