import { Injectable, Inject } from "@angular/core";
import {
    HttpClient,
    HttpParams,
    HttpRequest,
    HttpEventType,
    HttpHeaders,
    HttpResponse,
  } from "@angular/common/http";
import { Environment } from 'src/app/common/models/environment.model';
import { User } from 'src/app/common/models/user.model';

@Injectable()
export class ApiService{

    constructor(private http: HttpClient,
        @Inject('@@environment') private environment: Environment) { }

    getCurrencyRatesBySymbol(date: string, symbol: string): Promise<Object> {
        return this.http.get(this.environment.api + date + '?access_key=' +  this.environment.fixerApiKey + '&symbols=' + symbol).toPromise();
    }

}