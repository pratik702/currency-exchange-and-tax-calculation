import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './dashboard/dashboard.component';
import { AuthGuard } from './common/service/auth-guard.service';

const routes: Routes = [
  {path:  "", pathMatch:  "full",redirectTo:  "/dashboard", canActivate: [AuthGuard]},
  {path: "dashboard", component: DashboardComponent, canActivate: [AuthGuard] }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
