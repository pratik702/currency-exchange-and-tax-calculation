import { Component, OnInit } from '@angular/core';
import { ApiService } from '../common/service/api.service.';
import { Router } from '@angular/router';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { CalculatedResult } from '../common/models/calculated-result.model';
import { Currency } from '../common/models/currency.model';
import { NotificationService } from '../common/service/notification.service';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  inputForm: FormGroup;
  paymentOptions = [new Currency('Canadian Dollar', 'CAD', 1.564839, 11),
  new Currency('US Dollar', 'USD', 1.187247, 10),
  new Currency('Euro', 'EUR', 1, 9)];
  output: CalculatedResult = null;
  isCalculated: boolean = false;

  constructor(
    private apiService: ApiService,
    private notificationService: NotificationService,
    private datePipe: DatePipe
  ) { }

  ngOnInit(): void {
    this.inputForm = new FormGroup({
      invoiceDate: new FormControl('', [
        Validators.required
      ]),
      preTaxAmount: new FormControl('', [
        Validators.required
      ]),
      paymentCurrency: new FormControl('', [
        Validators.required
      ])
    });
  }

  calculate() {
    let invoiceDate = this.datePipe.transform(this.inputForm.controls.invoiceDate.value, "yyyy-MM-dd");
    let selectedCurrency = this.paymentOptions.filter(item => item.code == this.inputForm.controls.paymentCurrency.value)[0];
    let preTaxAmount = this.inputForm.controls.preTaxAmount.value * selectedCurrency.rate;
    let taxAmount = selectedCurrency.tax * preTaxAmount / 100;
    let total = preTaxAmount + taxAmount;
    let exchangeRate = selectedCurrency.rate;

    this.apiService.getCurrencyRatesBySymbol(invoiceDate, selectedCurrency.code)
      .then(response => {
        let onlineExchangeRate = response['rates'][selectedCurrency.code];
        this.output = new CalculatedResult(preTaxAmount, taxAmount, total, onlineExchangeRate);
        this.notificationService.success('Calculation Success', '');
      })
      .catch(error => {
        console.log(error);
        this.output = new CalculatedResult(preTaxAmount, taxAmount, total, exchangeRate);
        this.notificationService.warn('Unable to contact Fixer.io', 'Using offline rates');
      }).finally(() => {
        this.isCalculated = true;
      });

  }

}
