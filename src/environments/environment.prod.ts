import { Environment } from '../app/common/models/environment.model'

export const environment : Environment= {
  environmentName: 'production',
  production: true,
  api: 'http://data.fixer.io/api/',
  fixerApiKey: 'e754c4d6b3fc9f00bb66082ebdbfb3af'
};